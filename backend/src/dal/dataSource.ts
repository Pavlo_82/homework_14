import { DataSource } from "typeorm";
import * as Entities from "./entity/index.ts";

export const AppDataSource = new DataSource({
  type: "postgres",
  host: "localhost",
  port: 5432,
  username: "postgres",
  password: "1",
  database: "danit",
  logging: true,
  //entities: Entities,
  entities: ["src/dal/entity/*.ts"],
  synchronize: false,
  migrations: ["src/dal/migrations/*.ts"],
});
